package prueba.estrategias.users.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import prueba.estrategias.users.entity.Empresa;
import prueba.estrategias.users.entity.Usuario;

import java.util.List;

public interface UserRepository extends JpaRepository<Usuario, String> {

    public List<Usuario> findByNitEmpresa(Empresa nitEmpresa);

}
