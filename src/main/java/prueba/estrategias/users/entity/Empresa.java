package prueba.estrategias.users.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data @AllArgsConstructor @NoArgsConstructor @Builder
public class Empresa {

    @Id
    private String nit;
    private String nombre;
    private Integer estado;
    private String libre1;

}
