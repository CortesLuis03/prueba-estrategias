package prueba.estrategias.users.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor @NoArgsConstructor @Builder
public class Usuario {

    @Id
    private String cedula;
    private String primernombre;
    private String segundonombre;
    private String primerapellido;
    private String segundoapellido;
    private String clave;
    private String email;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nit")
    private Empresa nitempresa;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    private Roles rol;
    private Integer estado;

}
