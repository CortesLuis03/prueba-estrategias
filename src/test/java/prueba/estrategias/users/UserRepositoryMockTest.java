package prueba.estrategias.users;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import prueba.estrategias.users.entity.Empresa;
import prueba.estrategias.users.entity.Roles;
import prueba.estrategias.users.entity.Usuario;
import prueba.estrategias.users.repository.UserRepository;

import java.util.List;

@DataJpaTest
public class UserRepositoryMockTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void whenFindByEmpresa_thenReturnListUser(){

        Usuario user01 = Usuario.builder()
                .cedula("1107091203")
                .primernombre("Luis")
                .segundonombre("Eduardo")
                .primerapellido("Cortes")
                .segundoapellido("Rodriguez")
                .clave("123456")
                .email("lucho951003@gmail.com")
                .nitempresa(new Empresa())
                .rol(new Roles())
                .estado(1).build();

        userRepository.save(user01);

        List<Usuario> founds = userRepository.findByNitEmpresa(user01.getNitempresa());

//        Assertions.assertEquals(1, founds.size());

    }
}
